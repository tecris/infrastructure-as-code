## Infrastructure & continuous delivery with Docker on Ubuntu

 - [Provision](environment/ubuntu16.04/README.md) continuous delivery host environment
 - [Provision](cd-stack/README.md) continuous delivery stack
